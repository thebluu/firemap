import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Address, SearchAdapter } from '../models/address';
import { map } from "rxjs/operators";
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  public url: string = 'https://firemap-backend.herokuapp.com/adresses/?search=';
  public query: string


  constructor(
    private http: HttpClient,
    private searchAdapter: SearchAdapter,
  ) { }

  // list_mock(): Observable<Address[]> {
  //   return of(this.mock_plans).pipe(
  //     map((data: any[]) => data.map(item=>this.searchAdapter.adapt(item)))
  //   );
  // }

  /**
   * get list of plans from api
   */
  list(): Observable<Address[]> {
    console.log(this.url+this.query);
    
    return this.http.get<Address[]>(this.url+this.query).pipe(
      map((data: any) => data.results.map(item=>this.searchAdapter.adapt(item)))
    );
  }

  setQuery(query: string) {

    this.query = query;
  }

  getQuery(): string {
    
    return this.query;
  }
}

