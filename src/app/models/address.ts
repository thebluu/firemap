import { Injectable } from '@angular/core';
import { Adapter } from '../core/adapter';
import { GeoJson, GeoJsonAdapter } from './geo-json';

export class Address {
    constructor(
        public id: string,
        public title: string,
        public address: string,
        public image_url: string,
        public geojson: GeoJson,
    ) { }
}

@Injectable({
    providedIn: 'root'
})
  export class SearchAdapter implements Adapter<Address> {
    
    constructor(private geoJsonAdapter: GeoJsonAdapter){}

    adapt(item: any): Address {
        return new Address(
            item.id,
            item.nom_batiment,
            [item.numero_rue, item.nom_rue, item.code_postal, item.ville].join(' '),
            item.image_url,
            this.geoJsonAdapter.adapt([item.longitude, item.latitude]),
        );
    }
  }